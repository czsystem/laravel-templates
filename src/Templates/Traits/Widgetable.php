<?php

namespace CloudZentral\Templates\Traits;

use Illuminate\View\View;
use Throwable;

/**
 * Trait WidgetRenderable
 * @package CloudZentral\Templates\Traits
 */
trait Widgetable
{
    /**
     * Render a widget.
     * @param string|null $type
     * @param mixed ...$attributes
     * @return string|null
     * @throws Throwable
     */
    public function renderWidget(?string $type, ...$attributes): ?string
    {
        switch ($type) {
            case "text":
                return $this->renderTextWidget($attributes[0]);
            case "image":
                return $this->renderImageWidget($attributes[0], $attributes[1]);
            default:
                return null;
        }
    }

    /**
     * Get text widget view.
     * @param string|null $text
     * @return View
     */
    abstract public function getTextWidgetView(?string $text): View;

    /**
     * Render text widget view.
     * @param string|null $text
     * @return string|null
     * @throws Throwable
     */
    public function renderTextWidget(?string $text): ?string
    {
        return $this->getTextWidgetView($text)->render();
    }

    /**
     * Get image widget view.
     * @param string|null $alt
     * @param string|null $src
     * @return View
     */
    abstract public function getImageWidgetView(?string $alt, ?string $src, ?string $imagewidth): View;

    /**
     * Render image widget view.
     * @param string|null $alt
     * @param string|null $src
     * @return string|null
     * @throws Throwable
     */
    public function renderImageWidget(?string $alt, ?string $src, ?string $imagewidth): ?string
    {
        if( ! is_null($src) && ! empty($src) ) {
            $headers = get_headers($src);
            $exists = stripos($headers[0], "200 OK") ? true : false;

            if ($exists) {
                return $this->getImageWidgetView($alt, $src)->render();
            }
        }
        return null;
    }
}
