<?php

namespace CloudZentral\Templates\Interfaces;

use Illuminate\Contracts\View\Factory;
use Illuminate\View\View;

/**
 * Interface TemplateInterface
 * @package CloudZentral\Templates\Interfaces
 */
interface TemplateInterface
{
    /**
     * Render the view.
     * @param array $attributes
     * @return string
     */
    public function render(array $attributes = []): string;

    /**
     * Get the the view.
     * @param array $attributes
     * @return Factory|View
     */
    public function getView(array $attributes);

    /**
     * Get default attributes.
     * @return array
     */
    public function getDefaultAttributes(): array;
}
