<?php

namespace CloudZentral\Templates;

use CloudZentral\Templates\Interfaces\TemplateInterface;

/**
 * Class Template
 * @package CloudZentral\Templates
 */
abstract class Template implements TemplateInterface
{
    /**
     * Template constructor.
     */
    protected function __construct()
    {
    }

    /**
     * Make a template.
     * @param string $type
     * @return Template|null
     */
    abstract public static function make(string $type);

    /**
     * @inheritDoc
     * @throws \Throwable
     */
    public function render(array $attributes = []): string
    {
        $attributes = array_merge($this->getDefaultAttributes(), $attributes);
        $view = $this->getView($attributes);
        return $view->render();
    }
}
